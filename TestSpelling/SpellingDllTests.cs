﻿using System;
using Spelling.Dll;
using Xunit;

namespace TestSpelling
{
    
    public class SpellingDllTests
    {
        [Theory]
        [InlineData("4","")]
        public void DigitTest(string message, string expected)
        {
            ISpeller speller = new Speller(message);
            Assert.Equal(expected, speller.Spall());
        }

        [Theory]
        [InlineData("asd4", "277773")]
        public void CombinitedDigitalAndLeters(string message, string expected)
        {
            ISpeller speller = new Speller(message);
            Assert.Equal(expected, speller.Spall());
        }

        [Theory]
        [InlineData("aa", "2 2")]
        public void TwoEqualLeters(string message, string expected)
        {
            ISpeller speller = new Speller(message);
            Assert.Equal(expected, speller.Spall());
        }

        [Theory]
        [InlineData("asd asd", "2777730277773")]
        public void TwoWords(string message,string expected)
        {
            ISpeller speller = new Speller(message);
            Assert.Equal(expected, speller.Spall());
        }

        [Theory]
        [InlineData("foo  bar glvz", "333666 6660 022 2777045558889999")]
        public void ThreeWordsAndTwoSeparators(string message, string expected)
        {
            ISpeller speller = new Speller(message);
            Assert.Equal(expected, speller.Spall());
        }

        [Theory]
        [InlineData("", "")]
        public void BadDataset(string message, string expected)
        {
            ISpeller speller = new Speller(message);
            speller.DatasetType = new BadDataset(9999);
            Assert.Equal(expected, speller.Spall());
        }
    }
}
