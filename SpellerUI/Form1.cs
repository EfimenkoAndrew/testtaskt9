﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Spelling.Dll;
using Autofac;

namespace SpellerUI
{
    public partial class MainForm : Form
    {
        private Autofac.IContainer container;
        public MainForm()
        {
            InitializeComponent();
            var builder = new ContainerBuilder();
            builder.RegisterType<Speller>().AsSelf();
            builder.RegisterType<SmallDataset>().As<IDatasetType>();
            builder.RegisterType<LargeDataset>().As<IDatasetType>();
            builder.RegisterType<BadDataset>().As<IDatasetType>();
            container = builder.Build();
        }

        private void textBoxInput_TextChanged(object sender, EventArgs e)
        {
            using (var scope = container.BeginLifetimeScope())
            {
                int linesCount = textBoxInput.Lines.Length;
                List<string> AdditionalInfo = new List<string>(),
                    OutputMessage = new List<string>();
                string spalledLite = "";

                for (int i = 0; i < linesCount; i++)
                {
                    ISpeller speller = scope.Resolve<Speller>(new NamedParameter("inputString", textBoxInput.Lines[i]));
                    spalledLite = speller.Spall();
                    if (spalledLite == "")
                    {
                        spalledLite = "Bad input";
                    }
                    OutputMessage.Add($"Line #{i}: {spalledLite}");
                    AdditionalInfo.Add($"Line #{i}:");
                    AdditionalInfo.Add(speller.DatasetType.DatasetInfo);
                    AdditionalInfo.Add("Message lenght = " + speller.DatasetType.DatasetLentht);

                }
                textBoxAdditionalInfo.Lines = AdditionalInfo.ToArray();
                textBoxAdditionalInfo.SelectionStart = textBoxAdditionalInfo.Text.Length;
                textBoxAdditionalInfo.ScrollToCaret();
                textBoxOutput.Lines = OutputMessage.ToArray();
                textBoxOutput.SelectionStart = textBoxOutput.Text.Length;
                textBoxOutput.ScrollToCaret();
            }
            GC.Collect();
        }
    }
}
