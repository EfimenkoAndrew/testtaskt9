﻿namespace SpellerUI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelInputText = new System.Windows.Forms.Label();
            this.textBoxInput = new System.Windows.Forms.TextBox();
            this.textBoxOutput = new System.Windows.Forms.TextBox();
            this.labelResult = new System.Windows.Forms.Label();
            this.labelAdditionalInformation = new System.Windows.Forms.Label();
            this.textBoxAdditionalInfo = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // labelInputText
            // 
            this.labelInputText.AutoSize = true;
            this.labelInputText.Location = new System.Drawing.Point(9, 19);
            this.labelInputText.Name = "labelInputText";
            this.labelInputText.Size = new System.Drawing.Size(55, 13);
            this.labelInputText.TabIndex = 0;
            this.labelInputText.Text = "Input Text";
            // 
            // textBoxInput
            // 
            this.textBoxInput.Location = new System.Drawing.Point(12, 45);
            this.textBoxInput.Multiline = true;
            this.textBoxInput.Name = "textBoxInput";
            this.textBoxInput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxInput.Size = new System.Drawing.Size(632, 120);
            this.textBoxInput.TabIndex = 1;
            this.textBoxInput.TextChanged += new System.EventHandler(this.textBoxInput_TextChanged);
            // 
            // textBoxOutput
            // 
            this.textBoxOutput.Location = new System.Drawing.Point(12, 208);
            this.textBoxOutput.Multiline = true;
            this.textBoxOutput.Name = "textBoxOutput";
            this.textBoxOutput.ReadOnly = true;
            this.textBoxOutput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxOutput.Size = new System.Drawing.Size(632, 120);
            this.textBoxOutput.TabIndex = 3;
            // 
            // labelResult
            // 
            this.labelResult.AutoSize = true;
            this.labelResult.Location = new System.Drawing.Point(9, 182);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(72, 13);
            this.labelResult.TabIndex = 2;
            this.labelResult.Text = "Spelling result";
            // 
            // labelAdditionalInformation
            // 
            this.labelAdditionalInformation.AutoSize = true;
            this.labelAdditionalInformation.Location = new System.Drawing.Point(672, 19);
            this.labelAdditionalInformation.Name = "labelAdditionalInformation";
            this.labelAdditionalInformation.Size = new System.Drawing.Size(108, 13);
            this.labelAdditionalInformation.TabIndex = 4;
            this.labelAdditionalInformation.Text = "Additional Information";
            // 
            // textBoxAdditionalInfo
            // 
            this.textBoxAdditionalInfo.Location = new System.Drawing.Point(675, 45);
            this.textBoxAdditionalInfo.Multiline = true;
            this.textBoxAdditionalInfo.Name = "textBoxAdditionalInfo";
            this.textBoxAdditionalInfo.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxAdditionalInfo.Size = new System.Drawing.Size(363, 283);
            this.textBoxAdditionalInfo.TabIndex = 5;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1050, 340);
            this.Controls.Add(this.textBoxAdditionalInfo);
            this.Controls.Add(this.labelAdditionalInformation);
            this.Controls.Add(this.textBoxOutput);
            this.Controls.Add(this.labelResult);
            this.Controls.Add(this.textBoxInput);
            this.Controls.Add(this.labelInputText);
            this.Name = "MainForm";
            this.Text = "T9 Speller";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelInputText;
        private System.Windows.Forms.TextBox textBoxInput;
        private System.Windows.Forms.TextBox textBoxOutput;
        private System.Windows.Forms.Label labelResult;
        private System.Windows.Forms.Label labelAdditionalInformation;
        private System.Windows.Forms.TextBox textBoxAdditionalInfo;
    }
}

