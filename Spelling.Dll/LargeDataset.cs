﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spelling.Dll
{
    public class LargeDataset : IDatasetType
    {
        public LargeDataset(int datasetLenght)
        {
            DatasetInfo = "Large dataset 100 < N ≤ 1000.";
            DatasetLentht = datasetLenght;
        }
        public string DatasetInfo { get ; set ; }
        public int DatasetLentht { get ; set ; }
    }
}
