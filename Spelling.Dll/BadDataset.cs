﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spelling.Dll
{
    public class BadDataset : IDatasetType
    {
        public BadDataset(int datasetLenght)
        {
            DatasetInfo = "Bad dataset N > 1000 or N = 0";
            DatasetLentht = datasetLenght;
        }
        public string DatasetInfo { get ; set ; }
        public int DatasetLentht { get ; set ; }
    }
}
