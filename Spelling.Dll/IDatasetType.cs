﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spelling.Dll
{
    public interface IDatasetType
    {
        string DatasetInfo { get; set; }
        int DatasetLentht { get; set; }
    }
}
