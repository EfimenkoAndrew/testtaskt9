﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spelling.Dll
{
    public class SmallDataset : IDatasetType
    {
        public SmallDataset(int datasetLenght)
        {
            DatasetInfo = "Small dataset 1 ≤ N ≤ 100.";
            DatasetLentht = datasetLenght;
        }

        public string DatasetInfo { get; set; }
        public int DatasetLentht { get; set; }
    }
}
