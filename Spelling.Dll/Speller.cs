﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Spelling.Dll
{
    public class Speller : ISpeller
    {
        public IDatasetType DatasetType { get; set; }
        public string InputString { get; set; }
        private readonly Regex[] regex;

        public Speller(string inputString)
        {
            InputString = inputString;
            SetDataset();
            this.regex = new Regex[10] { new Regex(" "),
                new Regex("^$"), new Regex("[abc]"), new Regex("[def]"),
                new Regex("[ghi]"), new Regex("[jkl]"), new Regex("[mno]"),
                new Regex("[pqrs]"), new Regex("[tuv]"), new Regex("[wxyz]")};
        }

        public string Spall()
        {
            string rezSpall = "";
            int previosStep = -1, inpMessageLenght = InputString.Length;

            if (DatasetType is BadDataset)
            {
                return "";
            }

            for (int j = 0; j < inpMessageLenght; j++)
            {

                for (int i = 0; i < 10; i++)
                {
                    if (regex[i].IsMatch(InputString[j].ToString()) == true)
                    {
                        if (previosStep == i) rezSpall += " ";
                        previosStep = i;
                        int characterCount = -1;
                        switch (i)
                        {
                            case 0:
                                rezSpall += 0;
                                break;
                            case 2:
                                characterCount = "abc".IndexOf(InputString[j]) + 1;
                                for (int k = 0; k < characterCount; k++)
                                {
                                    rezSpall += 2;
                                }
                                break;
                            case 3:
                                characterCount = "def".IndexOf(InputString[j]) + 1;
                                for (int k = 0; k < characterCount; k++)
                                {
                                    rezSpall += 3;
                                }
                                break;
                            case 4:
                                characterCount = "ghi".IndexOf(InputString[j]) + 1;
                                for (int k = 0; k < characterCount; k++)
                                {
                                    rezSpall += 4;
                                }
                                break;
                            case 5:
                                characterCount = "jkl".IndexOf(InputString[j]) + 1;
                                for (int k = 0; k < characterCount; k++)
                                {
                                    rezSpall += 5;
                                }
                                break;
                            case 6:
                                characterCount = "mno".IndexOf(InputString[j]) + 1;
                                for (int k = 0; k < characterCount; k++)
                                {
                                    rezSpall += 6;
                                }
                                break;
                            case 7:
                                characterCount = "pqrs".IndexOf(InputString[j]) + 1;
                                for (int k = 0; k < characterCount; k++)
                                {
                                    rezSpall += 7;
                                }
                                break;
                            case 8:
                                characterCount = "tuv".IndexOf(InputString[j]) + 1;
                                for (int k = 0; k < characterCount; k++)
                                {
                                    rezSpall += 8;
                                }
                                break;
                            case 9:
                                characterCount = "wxyz".IndexOf(InputString[j]) + 1;
                                for (int k = 0; k < characterCount; k++)
                                {
                                    rezSpall += 9;
                                }
                                break;
                        }
                    }
                }
            }

            return rezSpall;
        }

        private void SetDataset()
        {
            int inputStrLem = InputString.Length;
            if (0 < inputStrLem && inputStrLem <= 100)
            {
                DatasetType = new SmallDataset(inputStrLem);
            }
            if (inputStrLem > 100)
            {
                DatasetType = new LargeDataset(inputStrLem);
            }
            if (inputStrLem > 1000 || inputStrLem == 0)
            {
                DatasetType = new BadDataset(inputStrLem);
            }
        }
    }
}
