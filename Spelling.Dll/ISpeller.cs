﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spelling.Dll
{
    public interface ISpeller
    {
        IDatasetType DatasetType { get; set; }
        string InputString { get; set; }
        string Spall();
    }
}
